## Welcome to the `golbclient`!

### Introduction
This project was developed at CERN with the contribution of CERN's load-balancing team, contactable at [`lb-experts@cern.ch`](mailto:lb-experts@cern.ch). The official code repository can be found at [cernops/golbclient](https://github.com/cernops/golbclient) and is currently being maintained by both me and the team at CERN. However, the repositories are not synchronized and no future joint-effort is guaranteed.

### Overview
In this section, an overview is given over the implementation of a metrics agent/probe that runs on either a VM or container and executes certain configurable checks and outputs a metric value. Each agent can be configured with a list of supplying DNS aliases which by themselves has a list of metrics that will be evaluated against.

Currently the agent has support for the following metric types:

| Metric type | Description |
| ------ | --- |
| `nologin` | Checks that the file `nologin` is not present |
| `tmpfull` | Checks that the `/tmp` directory is not full |
| `sshdaemon` | Check if the port `22` is open and listening for connections |
| `webdaemon` | Check if the port `80` is open and listening for connections |
| `ftpdaemon` | Check if the port `21` is open and listening for connections |
| `gridftpdaemon` | Check if the port `2811` is open and listening for connections |
| `afs` | Checks if CERN's `afs` mounting point is available |
| `lemon` | Evaluates a c-style expression using lemon metrics |
| `collectd` | Evaluates a c-style expression using collectd metrics |
| `collectd_alarms` | Checks if the desired `collectd` metrics have the supplied alarm state |
| `constant` | Loads into the final metric value a constant |
| `daemon` | Check if the given map of protocol(s), IP version(s) and port(s) are in open and listening states |

### Achievements
* Chep 2018 - Paper [link](https://doi.org/10.1051/epjconf/201921408028)
* Chep 2019 - Paper [link]() -> Coming soon!


### Application arguments
```bash
Usage:
  lbclient [OPTIONS]

Application Options:
  -l, --log=                   Location of the of log file (default: log/app.log)
  -d, --loglevel=              Console debug level [TRACE, DEBUG, INFO, WARN, ERROR, FATAL] (default: FATAL)
      --fdevel=                File debug level [TRACE, DEBUG, INFO, WARN, ERROR, FATAL] (default: TRACE)
      --flog                   Activate the file logging service
      --cm=                    Set the directory where the client should fetch the configuration files from (default: /usr/local/etc/)
      --ca=                    Set an alternative path for the lbaliases configuration file (default: /usr/local/etc/lbaliases)
  -c, --conf-name=             Set the default name to be used to lookup for the generic configuration file (default: lbclient.conf)
  -v, --version                Version of the file

rotatecfg:
      --rotatecfg.enabled      Enable the automatic rotation of the log files. (default: false)
      --rotatecfg.linelimit=   The maximum amount of lines per log file. (default: 5000)
      --rotatecfg.backuplimit= The maximum amount of backups. (default: 9)
      --rotatecfg.msgBuffer=   The message buffer capacity. (default: 100)

Help Options:
  -h, --help                   Show this help message
```
